package com.google.samples.erickrojasperez.currencyapp;

/**
 * Created by erickrojasperez on 11/9/16.
 */

public class Constants {
    // URL for retrieval of currency exchange rates
    public static final String Currency_URL = "http://api.fixer.io/lastest?base=";

    // Constants for parsing currency from json response
    public static final  String BASE = "base";
    public static final  String Date = "date";
    public static final  String RATES = "rates";

    // Constants used for CurrencyService and receiver
    public static final String URL = "url";
    public static final String RECIEVER = "reciever";
    public static final String RESULT = "result";
    public static final String CURRENCY_BASE = "currencyBase";
    public static final String CURRENCY_NAME = "currencyName";
    public static final String REQUEST_ID = "requestId";
    public static final String BUNDLE = "bundle";
    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STRATUS_ERROR = 2;

    // Constants used for Database and table
    public static final String DATABASE_NAME = "CurrencyDB";

    public static final String CURRENCY_TABLE = "currency";
    public static final String KEY_ID = "id";
    public static final String KEY_BASE = "date";
    public static final String KEY_DATE = "rate";
    public static final String KEY_NAME = "name";

    // Maximum number of retrievals running in background
    public static final int MAX_DOWNLOADS = 5;

    // Constants for all Cureency Code and Name
    public static final String[] CURRENCY_CODES = {
            "AUD", "BGN", "BRL", "CAD", "CHF", "CNY", "CZK", "DKK", "EUR", "GBP", "HDK", "HUF", "IDR", "ILS", "INR", "JPY",
            "KRW", "MXN", "MYR", "NOK", "NZD", "PHP", "PLN", "RON", "RUB", "SEK", "SGD", "THB", "USD", "ZAR"
    };

    public static final String[] CURRENCY_NAMES = {
            "Australian Dollar", "Bulgarian Lev", "Brazilian Real", "Canadian Dollar", "Swiss Franc",
            "Yuan Renminbi", "Czech Koruna", "Danish Krone", "Euro", "Pound Sterling", "Honk Kong Dollar",
            "Croatian Kuna", "Hungarian Froint", "Indonesian Rupiah", "Israeli New Shekel", "Indian Rupee",
            "Japanese Yen", "Korean Won", "Mexican Nuevo Peso", "Malaysian Ringgit", "Norwegian Krone",
            "New Zealand Dollar", "Philippine Peso", "Polish Zloty", "Romanian New Leu", "Belarussian Ruble",
            "Swedish Krona", "Singapore Dollar", "Thai Baht", "Turkish Lira", "US Dollar", "South African Rand"
    };

    // Constant for notification
    public static final  int NOTIFICATION_ID = 100;

    // Constants for SharedPreferences
    public static final String CURRENCY_PREFERENCES = "CURRENCY_PREFERENCES";
    public static final String BASE_CURRENCY = "BASE_CURRENCY";
    public static final String TARGET_CURRENCY = "TARGET_CURRENCY";
    public static final String SERVICE_REPETITION = "SERVICE_REPETITION";
    public static final String NUM_DOWNLOADS = "NUM_DOWNLOADS";





}
