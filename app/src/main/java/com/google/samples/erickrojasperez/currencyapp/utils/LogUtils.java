package com.google.samples.erickrojasperez.currencyapp.utils;

import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.util.Log;

import static android.R.attr.format;

/**
 * Created by erickrojasperez on 11/9/16.
 */

public class LogUtils {

    private static StringBuffer sStringBuffer = new StringBuffer();

    public interface LogListener{
        void onLogged(String log);
        //
    }

    private static LogListener sLogListener;

    public static void log(String tag, String message) {
        Log.d(tag, message);
        StringBuilder stringBuilder = new StringBuilder();
        String date formatDate(Calendar.getInstance());
        stringBuilder.append(date);
        stringBuilder.append(" ");
        stringBuilder.append(tag);
        stringBuilder.append("");
        stringBuilder.append(message);
        stringBuilder.append("\n\n");
        sStringBuffer.insert(0, stringBuilder.toString()); // try to catch the final variable inthe control system
        printLogs();

    }

    private static String formatDate(Calendar calendar){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-hh:mm:ss");
        return simpleDateFormat.format(calendar.getTime());
    }

    public static void printLogs(){
      if (sLogListener != null){
          sLogListener.onLogged(sStringBuffer);
      }
    }

}
